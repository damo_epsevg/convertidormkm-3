package damo.cs.upc.edu.convertidormkm;

/**
 *  Captura dels events del RadioGrouo enlloc dels events dels RadioButton
 */

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

public class ConvertidorMKM extends Activity {
    public static final double FACTOR_DE_CONVERSIO = 1.609344;
    private RadioGroup selector;
    private EditText text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convertidor_mkm);
        inicialitza();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.convertidor_mkm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void inicialitza() {
        text =  findViewById(R.id.editText);

        selector =  findViewById(R.id.seleccioUnitats);

        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                esborrar(view);
            }
        });

        selector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conversio(v);
            }
        });
    }



    private void conversio(View view) {
       float inputValue;

        try {
            inputValue = Float.parseFloat(text.getText().toString());
        }
        catch (Exception e){
            return;
        };

        switch (selector.getCheckedRadioButtonId()){
            case R.id.radioButtonKm:
                text.setText(String.valueOf(inputValue / FACTOR_DE_CONVERSIO));
                selector.check(R.id.radioButtonMilles);
               break;
            case R.id.radioButtonMilles:
                text.setText(String.valueOf(inputValue * FACTOR_DE_CONVERSIO));
                selector.check(R.id.radioButtonKm);
                break;
        }
    }

    private void esborrar(View v){
        ((TextView) v).setText("");
    }

}
